.. title: USSR22
.. slug: ussr22
.. date: 2022-05-07 13:45:50 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

.. image:: /images/licorne.jpg
   :target: /images/licorne.jpg
   :class: thumbnail
   :alt: une licorne
  

Unicorn Summer Solstice Ride, édition 2022 : 100km à plat et en site propre, de nuit, pour aller voir le lever de soleil sur la Baie de Somme.

- 📆 Nuit du 18 au 19 juin 2022
- 🌇 Départ au coucher du soleil à 22h02
- 🌒 Lune décroissante
- 🌅 Lever du soleil à 5h41
- 🌤 Températures prévues : Données disponibles sous peu.

 `Formulaire d'inscription USSR22 <https://www.helloasso.com/associations/unicorn-chain-gang/evenements/ussr22/>`_.

Itinéraire
----------

- Regroupement : Gare SNCF Amiens à partir de 18h
- Départ : Oppidum Samara à 22h
- Amiens › Abbeville › Saint-Valéry par la `Véloroute de la Vallée de la Somme V30 <https://www.google.com/maps/d/u/0/viewer?mid=1lNauDLlAhhkhcQ2vlPU24IFCrj8&ll=49.9173156663518%2C2.5256109500000257&z=10>`_.
  {{% template %}}
  <iframe src="https://www.google.com/maps/d/embed?mid=1lNauDLlAhhkhcQ2vlPU24IFCrj8&ehbc=2E312F" width="640" height="480" style="max-width:100%"></iframe>
  {{% /template %}}
- Saint-Valéry › Le Hourdel › Cayeux par la `Vélomaritime EV4 <https://www.lavelomaritime.fr/itineraire/cayeux-sur-mer-le-crotoy>`_.
  {{% template %}}
  <iframe src="https://www.google.com/maps/d/u/0/embed?mid=1ifQc2VH2hZ9GwzQ8tNS_fpu0qYfhpO6_&ehbc=2E312F" width="640" height="480" style="max-width:100%"></iframe>
  {{% /template %}}
- Ravitaillement à Abbeville.
- Petit-déjeuner prévu à la boulangerie à la fin de la ballade à Saint Valery/Somme, voir formulaire d'inscription.
- Retour possible en train vers Amiens: une gare tous les 12km entre Noyelles-sur-Mer et Amiens (Noyelles-sur-Mer, Abbeville, Pont-Rémy, Fontaine-sur-Somme, Long-le-Catelet, Longpré les Corps Saints, Hangest-sur-Somme, Picquigny, Ailly-sur-Somme). `Fiche horaire SNCF de la ligne <https://www.ter.sncf.com/medias/PDF/hauts_de_france/FH_Amiens_Abbeville_Calais_du_30_mai_au_12_juin_2022_tcm77-310549_tcm77-310548.pdf>`_.

Préparation de l'évènement
--------------------------

- La reconnaissance **de jour** par Johan 
  {{% template %}} 
  <blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Reco pour l&#39;<a href="https://twitter.com/hashtag/USSR21?src=hash&amp;ref_src=twsrc%5Etfw">#USSR21</a> c&#39;est parti ! <a href="https://t.co/l54zEIt9vo">pic.twitter.com/l54zEIt9vo</a></p>&mdash; Johan des vélos - UCG (@JohanD_Velo) <a href="https://twitter.com/JohanD_Velo/status/1393796353118113793?ref_src=twsrc%5Etfw">May 16, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  {{% /template  %}}

Check-list
----------

- ☑︎ 🔦 Éclairage en bon état avant + arrière **non clignotant**

  - ☑︎ 🔦 une lampe frontale ou une petite lampe torche (c'est optionnel).
- ☑︎ 🥐 Un truc à manger, ne pas oublier ses gourdes, ne pas oublier de les remplir avant le départ.
- ☑︎ 🦸 Poncho + veste dans la sacoche — température mini annoncée 14°, s’attendre à 11-12° dans l’humidité de la nuit
- ☑︎ 🦺 Un gilet rétro-réflechissant — obligatoire de nuit hors agglo, c'est la loi
- ☑︎ 🚴🏽 Un vélo fonctionnel et révisé — pas question de faire de la mécanique à 2h du matin dans le noir au bord du canal
- ☒ 💉 Dopage interdit, sorry ¯\_(ツ)_/¯ 


 `Formulaire d'inscription USSR22 <https://www.helloasso.com/associations/unicorn-chain-gang/evenements/ussr22/>`_.
