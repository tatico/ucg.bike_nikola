.. title: Foire Aux Questions
.. slug: foire-aux-questions
.. date: 2022-05-13 15:56:15 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

Foire aux questions
-------------------

- Types de vélos acceptés:
  Tout vélo en état de faire 100km #pasOuf, des byciclettes de sécurité, des longtails, des Brompton, des gravel. On a jamais essayé avec le Bullitt, mais c'est pas interdit.
- Pauses:
  On fait des pauses courtes toutes les heures et deux pauses plus longes en fonction de l'heure, la fatigue et/ou la météo.
- Crevaisons:
  Il est interdit de crever ;)

